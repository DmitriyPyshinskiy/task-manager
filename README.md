# Task-manager
Applications for managing projects and tasks.

## Required software:
+ JRE 1.8
+ Apache Maven 3.4
## Technology stack:
+ JDK 1.8
+ Apache Maven 3.4
## The name of the developer and contacts:
Name: Pyshinskiy Dmitriy; email: dmitriy.pyshinskiy@mail.ru

## Command for building project:
````
mvn install
````
## Command for running application:
````
java -jar target/artifactId-version-SNAPSHOT.jar
````