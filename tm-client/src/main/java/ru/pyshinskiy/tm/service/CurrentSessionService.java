package ru.pyshinskiy.tm.service;

import lombok.Getter;
import lombok.Setter;
import ru.pyshinskiy.tm.endpoint.Session;

@Getter
@Setter
public class CurrentSessionService {

    private Session currentSession;
}
