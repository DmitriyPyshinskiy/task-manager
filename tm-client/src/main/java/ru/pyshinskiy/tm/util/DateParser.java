package ru.pyshinskiy.tm.util;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateParser {

    public static Date parseXMLGregorianCalendarToDate(XMLGregorianCalendar xgc) {
        return new Date(xgc.toGregorianCalendar().getTimeInMillis());
    }

    public static XMLGregorianCalendar parseDateToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        return xmlCalendar;
    }

}
