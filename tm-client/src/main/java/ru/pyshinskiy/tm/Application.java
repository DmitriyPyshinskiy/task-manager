package ru.pyshinskiy.tm;


import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.command.*;
import ru.pyshinskiy.tm.endpoint.Exception_Exception;
import sun.reflect.Reflection;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Set;

public class Application {

    @NotNull
    private static final Set<Class<? extends AbstractCommand>> COMMANDS = new Reflections().getSubTypesOf(AbstractCommand.class);


    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, JAXBException, Exception_Exception {
//        try {
//
        new Bootstrap().init(COMMANDS);
//        } catch(Exception e) {
//            System.out.println("Произошел сбой работы task-manager. Исключение: " + e.getClass());
//        }



    }
}