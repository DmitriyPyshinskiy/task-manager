package ru.pyshinskiy.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.*;
import ru.pyshinskiy.tm.endpoint.impl.*;
import ru.pyshinskiy.tm.service.CurrentSessionService;
import ru.pyshinskiy.tm.service.TerminalService;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final TerminalService terminalService = new TerminalService();
    @NotNull
    private final IProjectEndpoint projectService = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    private final ITaskEndpoint taskService = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    private final IUserEndpoint userService = new UserEndpointService().getUserEndpointPort();
    @NotNull
    private final ISessionEndpoint sessionService = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    private final Map<String, AbstractCommand> commandList = new LinkedHashMap<>();
    @NotNull
    private final CurrentSessionService currentSessionService = new CurrentSessionService();


    public void init(@NotNull Set<Class<? extends AbstractCommand>> COMMANDS) throws IOException, JAXBException, InstantiationException, IllegalAccessException, Exception_Exception {

        initCommands(COMMANDS);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            System.out.println("Введите команду: ");
            String inputCommand = terminalService.nextLine();
            if(inputCommand.equals("exit")) {
                System.out.println("Работа task-manager завершена.");
                break;
            }
            if(!commandList.containsKey(inputCommand)) {
                System.out.println("введённой команды не существует.");
                continue;
            }

            commandList.get(inputCommand).execute();
        }
    }

    private void initCommands(@NotNull Set<Class<? extends AbstractCommand>> COMMANDS) throws IllegalAccessException, InstantiationException {
        for(Class<? extends AbstractCommand> clazz: COMMANDS) {
            registry((AbstractCommand) clazz.newInstance());
        }
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setBootstrap(this);
        commandList.put(command.getName(), command);

    }

}
