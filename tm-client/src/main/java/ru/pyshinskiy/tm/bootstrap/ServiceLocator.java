package ru.pyshinskiy.tm.bootstrap;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.*;
import ru.pyshinskiy.tm.service.CurrentSessionService;
import ru.pyshinskiy.tm.service.TerminalService;

import java.util.Map;

public interface ServiceLocator {


    IProjectEndpoint getProjectService();

    ITaskEndpoint getTaskService();

    IUserEndpoint getUserService();

    ISessionEndpoint getSessionService();

    TerminalService getTerminalService();

    CurrentSessionService getCurrentSessionService();

    Map<String, AbstractCommand> getCommandList();

}
