package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.util.DateParser;


import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_create";
    }

    @Override
    public String getDescription() {
        return "создаёт проект.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        System.out.println("Начало инициализации проекта...");
        System.out.println("Введите имя проекта:");
        String projectName = terminalService.nextLine();
        System.out.println("Введите описание проекта:");
        String projectDescription = terminalService.nextLine();
        System.out.println("Введите дату начала проекта в формате - дд/мм/гггг:");
        try {
            Date dateBegin = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
            System.out.println("Введите дату окончания проекта в формате - дд/мм/гггг:");
            Date dateEnd = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
            Project project = new Project();
            project.setId(UUID.randomUUID().toString());
            project.setName(projectName);
            project.setDescription(projectDescription);
            project.setCreateTime(new Date().getTime());
            project.setDateBegin(DateParser.parseDateToXMLGregorianCalendar(dateBegin));
            project.setDateEnd(DateParser.parseDateToXMLGregorianCalendar(dateEnd));
            project.setUserId(currentSession.getUserId());
            bootstrap.getProjectService().persistProject(currentSession, project);
            terminalService.print("Проект успешно добавлен в локальную базу данных.");
        } catch (ParseException e) {
            terminalService.print("Вы ввели дату в неправильном формате.");
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

    }
}
