package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.StatusReadiness;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.util.DateParser;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_edit";
    }

    @Override
    public String getDescription() {
        return "позволяет редактировать проект.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        IProjectEndpoint projectService  = bootstrap.getProjectService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Какой проект вы хотите изменить? Введите название:");
        String projectName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if(projectService.doesProjectExistById(currentSession, projectName, userId)) {
            for (Project project : projectService.findAllProjectsById(currentSession, userId)) {
                if (project.getName() != null && project.getName().equals(projectName)) {
                    terminalService.print("Какие изменения вы хотите внести? Выберете цифру");
                    terminalService.print("1. Изменить имя проекта.");
                    terminalService.print("2. Изменить описание прокта.");
                    terminalService.print("3. Изменить дату окончания проекта.");
                    terminalService.print("4. Изменить статус готовности проекта.");

                    String input = terminalService.nextLine();
                    switch (input) {
                        case "1":
                            terminalService.print("Введите новое имя проекта:");
                            String newProjectName = terminalService.nextLine();
                            project.setName(newProjectName);
                            terminalService.print("Имя проекта успешно изменено.");
                            break;
                        case "2":
                            terminalService.print("Введите новое описание проекта:");
                            String newProjectDescription = terminalService.nextLine();
                            project.setDescription(newProjectDescription);
                            terminalService.print("Описание проекта успешно изменено.");
                            break;
                        case "3":
                            terminalService.print("Введите новую дату окончания проекта в формате - дд/мм/гггг:");
                            try {
                                Date newEndDate = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
                                project.setDateEnd(DateParser.parseDateToXMLGregorianCalendar(newEndDate));
                                terminalService.print("Дата окончания проекта успешно изменена");
                            } catch (ParseException e) {
                                terminalService.print("Вы ввели дату в неправильном формате.");
                            } catch (DatatypeConfigurationException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "4":
                            terminalService.print("Текущий статус готовности проекта: " + project.getStatusReadiness());
                            terminalService.print("Выберете новый статус проекта: \n 1 - В процессе. \n 2 - Завершён.");
                            String s = terminalService.nextLine();
                            if("1".equals(s)) {
                                project.setStatusReadiness(StatusReadiness.PROCESS);
                                terminalService.print("Статус проекта успешно изменён");
                            }
                            else if ("2".equals(s)) {
                                project.setStatusReadiness(StatusReadiness.DONE);
                                terminalService.print("Статус проекта успешно изменён");
                            }
                            else {
                                terminalService.print("Вы ввели некорректную цифру.");
                            }
                            break;
                    }
                }
            }
        }
        else {
            terminalService.print("Проекта с таким именем не существует.");
        }
    }
}
