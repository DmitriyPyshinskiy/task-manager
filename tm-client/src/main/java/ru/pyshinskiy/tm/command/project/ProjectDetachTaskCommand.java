package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectDetachTaskCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_detach_task";
    }

    @Override
    public String getDescription() {
        return "открепляет указанную задачу от проекта.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        System.out.println("Введите имя проекта:");
        String projectName = terminalService.nextLine();
        System.out.println("Введите имя задачи, которую вы хотите открепить:");
        String taskName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        try {
            bootstrap.getProjectService().detachProjectTask(currentSession
                    ,bootstrap.getProjectService().findOneProjectByNameAndId(currentSession, projectName, userId)
                    ,bootstrap.getTaskService().findOneTaskByNameAndId(currentSession, taskName, userId));
            terminalService.print("Задача была успешно откреплена от проекта.");
        } catch (Exception e) {
            terminalService.print("Введены некорректные данные.");
        }
    }
}
