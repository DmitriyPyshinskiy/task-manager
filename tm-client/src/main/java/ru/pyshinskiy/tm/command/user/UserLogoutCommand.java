package ru.pyshinskiy.tm.command.user;


import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "выход из системы";
    }

    @Override
    public void execute() {
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            System.out.println("Защищённая команда. Войдите в систему.");
            return;
        }
        System.out.println("Вы вышли из системы.");
        bootstrap.getCurrentSessionService().setCurrentSession(null);
    }
}
