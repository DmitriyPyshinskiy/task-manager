package ru.pyshinskiy.tm.command.task;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.StatusReadiness;
import ru.pyshinskiy.tm.endpoint.Task;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.util.DateParser;
//import ru.pyshinskiy.tm.entity.Task;
//import ru.pyshinskiy.tm.enums.StatusReadiness;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task_edit";
    }

    @Override
    public String getDescription() {
        return "позволяет редактировать задачу.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

       terminalService.print("Какую задачу вы хотите изменить? Введите название.");
        String taskName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if(bootstrap.getTaskService().doesTaskExistById(currentSession, taskName, userId)) {
            for (Task task : bootstrap.getTaskService().findAllTasksById(currentSession, userId)) {
                if (task.getName() != null && task.getName().equals(taskName)) {
                    terminalService.print("Какие изменения вы хотите внести? Выберете цифру");
                    terminalService.print("1. Изменить имя задачи.");
                    terminalService.print("2. Изменить описание задачи.");
                    terminalService.print("3. Изменить дату окончания задачи.");
                    terminalService.print("4. Изменить статус готовности задачи.");

                    String input = terminalService.nextLine();
                    switch (input) {
                        case "1":
                            terminalService.print("Введите новое имя задчи:");
                            String newTaskName = terminalService.nextLine();
                            task.setName(newTaskName);
                            terminalService.print("Имя задачи успешно изменено.");
                            break;
                        case "2":
                            terminalService.print("Введите новое описание задачи:");
                            String newTaskDescription = terminalService.nextLine();
                            task.setDescription(newTaskDescription);
                            terminalService.print("Описание задачи успешно изменено.");
                            break;
                        case "3":
                            terminalService.print("Введите новую дату окончания задачи в формате - дд/мм/гггг:");
                            try {
                                Date newEndDate = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
                                task.setDateEnd(DateParser.parseDateToXMLGregorianCalendar(newEndDate));
                                terminalService.print("Дата окончания задачи успешно изменена.");
                            } catch (ParseException e) {
                                System.err.println("Вы ввели дату в неправильном формате.");
                            } catch (DatatypeConfigurationException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "4":
                            terminalService.print("Текущий статус готовности задачи: " + task.getStatusReadiness());
                            terminalService.print("Выберете новый статус задачи: \n 1 - В процессе. \n 2 - Завершёна.");
                            String s = terminalService.nextLine();
                            if ("1".equals(s)) {
                                task.setStatusReadiness(StatusReadiness.PROCESS);
                                terminalService.print("Статус задачи успешно изменён");
                            } else if ("2".equals(s)) {
                                task.setStatusReadiness(StatusReadiness.DONE);
                                terminalService.print("Статус задачи успешно изменён");
                            } else {
                                terminalService.print("Вы ввели некорректную цифру.");
                            }
                            break;
                    }
                }
            }
        }
        else {
            terminalService.print("Задачи с таким именем не существует.");
        }
    }
}
