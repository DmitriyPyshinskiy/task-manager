package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.Task;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectTaskListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_task_list";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список всех задач, прикреплённых к проекту.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        IProjectEndpoint projectService = bootstrap.getProjectService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Введите имя проекта, список задач которого вы хотите посмотреть.");
        String projectName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if(projectService.doesProjectExistById(currentSession, projectName, userId)) {
            for (Project project : projectService.findAllProjectsById(currentSession, userId)) {
                if(project.getName().equals(projectName)) {
                    if(projectService.returnProjectListTasks(currentSession, project).size() == 0) {
                        terminalService.print("У проекта нет активных задач.");
                    }
                    else {
                        terminalService.print("Список активных задач проекта:");
                        showAllTasks(project);
                    }
                }
            }
        }
        else {
            terminalService.print("Проекта с таким именем не существует.");
        }
    }

    private void showAllTasks(Project project) {
        for (Task task : project.getTasks()) {
            System.out.println(task.getName());
        }
    }
}
