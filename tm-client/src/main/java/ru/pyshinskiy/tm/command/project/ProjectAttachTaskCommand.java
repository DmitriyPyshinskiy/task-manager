package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectAttachTaskCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_attach_task";
    }

    @Override
    public String getDescription() {
        return "прикрепляет указанную задачу к проекту.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        IProjectEndpoint projectService = bootstrap.getProjectService();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Введите имя проекта:");
        String projectName = terminalService.nextLine();
        terminalService.print("Введите имя задачи:");
        String taskName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        try {
            projectService.attachProjectTask(currentSession, projectService.findOneProjectByNameAndId(currentSession, projectName, userId)
                    ,bootstrap.getTaskService().findOneTaskByNameAndId(currentSession, taskName, userId));
            terminalService.print("Задача была успешно прикреплена к проекту.");
        } catch (Exception e) {
            terminalService.print("Введены некорректные данные.");
        }
    }
}
