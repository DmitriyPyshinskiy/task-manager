package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.endpoint.Goal;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class FindCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "find";
    }

    @Override
    public String getDescription() {
        return "ищет по названию, описанию или по их части проект или задачу.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }


        List<Goal> list = find(currentSession, terminalService);
        if(list.size() == 0) {
            terminalService.print("Совпадений не найдено.");
        }
        else {
            for (Goal goal : list) {

                if(goal instanceof Project) {
                    terminalService.print("Найдено совпадение.");
                    terminalService.print("ПРОЕКТ: " + goal.getName() + "\n" +
                            "ОПИСАНИЕ ПРОЕКТА: " + goal.getDescription());
                }
                else {
                    terminalService.print("Найдено совпадение.");
                    terminalService.print("ЗАДАЧА: " + goal.getName() + "\n" +
                            "ОПИСАНИЕ ЗАДАЧИ: " + goal.getDescription());
                }
            }
        }
    }


    private List<Goal> find(Session currentSession, TerminalService terminalService) throws IOException {
        List<Goal> list = new ArrayList<>();
        terminalService.print("Введите информацию о проекте или задаче:");

        String input = terminalService.nextLine();

        if(bootstrap.getProjectService().findOneProjectByNamePart(currentSession, input) !=null) {
            list.add(bootstrap.getProjectService().findOneProjectByNamePart(currentSession, input));
        }
        if(bootstrap.getProjectService().findOneProjectByDescriptionPart(currentSession, input) !=null) {
            list.add(bootstrap.getProjectService().findOneProjectByDescriptionPart(currentSession, input));
        }
        if(bootstrap.getTaskService().findOneTaskByNamePart(currentSession, input) !=null) {
            list.add(bootstrap.getTaskService().findOneTaskByNamePart(currentSession, input));
        }
        if(bootstrap.getTaskService().findOneTaskByDescriptionPart(currentSession, input) !=null) {
            list.add(bootstrap.getTaskService().findOneTaskByDescriptionPart(currentSession, input));
        }

        return list;
    }
}
