package ru.pyshinskiy.tm.command.user;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.User;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "change_password";
    }

    @Override
    public String getDescription() {
        return "смена пароля пользователя.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }


        User currentUser = bootstrap.getUserService().findOneUser(currentSession, currentSession.getUserId());
        terminalService.print("Введите новый пароль:");
        String newPassword1 = terminalService.nextLine();
        terminalService.print("Повторите новый пароль:");
        String newPassword2 = terminalService.nextLine();
        if(newPassword1.equals(newPassword2)) {

            currentUser.setHashPassword(DigestUtils.md5Hex(newPassword2));
        }
        else {
            terminalService.print("Пароли не совпадают.");
        }
    }
}
