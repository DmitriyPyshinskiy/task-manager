package ru.pyshinskiy.tm.command.task;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.Task;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task_list";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список всех активных задач.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        String userId = currentSession.getUserId();
        List<Task> taskList = bootstrap.getTaskService().findAllTasksById(currentSession, userId);

        if(bootstrap.getTaskService().findAllTasksById(currentSession, userId).size() == 0) {
            terminalService.print("У вас нет активных задач.");
            return;
        }


        terminalService.print("Выберете, в каком порядке вывести задачи на экран:\n" +
                "1 - В порядке создания в системе.\n" +
                "2 - По дате начала задачи.\n" +
                "3 - По дате окончания задачи.\n" +
                "4 - По статусу готовности.");

        String userInput = terminalService.nextLine();
        switch (userInput) {
            case "1":
                for (Task task : bootstrap.getTaskService().sortTaskByCreationDate(currentSession, taskList)) {
                    terminalService.print("Задача: " + task.getName() + " создан: " + new Date(task.getCreateTime()).toString());
                }
                break;
            case "2":
                for (Task task : bootstrap.getTaskService().sortTaskByStartDate(currentSession, taskList)) {
                    terminalService.print("Задача: " + task.getName() + " дата начала: " + Objects.requireNonNull(task.getDateBegin()).toString());
                }
                break;
            case "3":
                for (Task task : bootstrap.getTaskService().sortTaskByEndDate(currentSession, taskList)) {
                    terminalService.print("Задача: " + task.getName() + " дата окончания: " + Objects.requireNonNull(task.getDateEnd()).toString());
                }
                break;
            case "4":
                for (Task task : bootstrap.getTaskService().sortTaskByStatusReadiness(currentSession, taskList)) {
                    terminalService.print("Задача: " + task.getName() + " статус готовности: " + task.getStatusReadiness());
                }
                break;
            default:
                terminalService.print("Вы ввели неправильную цифру.");
                break;
        }
    }
}
