package ru.pyshinskiy.tm.command.task;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.Task;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.util.DateParser;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task_create";
    }

    @Override
    public String getDescription() {
        return "создаёт задачу.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Начало инициализации задачи...");
        terminalService.print("Введите имя задачи:");
        String taskName = terminalService.nextLine();
        terminalService.print("Введите описание задачи:");
        String taskDescription = terminalService.nextLine();
        terminalService.print("Введите дату создания задачи в формате - дд/мм/гггг:");
        try {
            Date dateBegin = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
            System.out.println("Введите дату выполнения задачи в формате - дд/мм/гггг:");
            Date dateEnd = new SimpleDateFormat("dd/MM/yyyy").parse(terminalService.nextLine());
            Task task = new Task();
            task.setId(UUID.randomUUID().toString());
            task.setName(taskName);
            task.setDescription(taskDescription);
            task.setCreateTime(new Date().getTime());
            task.setDateBegin(DateParser.parseDateToXMLGregorianCalendar(dateBegin));
            task.setDateEnd(DateParser.parseDateToXMLGregorianCalendar(dateEnd));
            task.setUserId(currentSession.getUserId());
            bootstrap.getTaskService().persistTask(currentSession, task);
            terminalService.print("Задача успешно добавлена в локальную базу данных.");
        } catch (ParseException e) {
            terminalService.print("Вы ввели дату в неправильном формате.");
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }
}
