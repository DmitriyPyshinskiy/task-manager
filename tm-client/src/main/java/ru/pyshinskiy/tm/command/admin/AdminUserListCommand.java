package ru.pyshinskiy.tm.command.admin;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.TypeOfRole;
import ru.pyshinskiy.tm.endpoint.User;
import ru.pyshinskiy.tm.service.TerminalService;

@NoArgsConstructor
public final class AdminUserListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user_list";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список всех пользователей.";
    }

    @Override
    public void execute() {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        if(!currentSession.getUserRole().equals(TypeOfRole.ADMIN)) {
            terminalService.print("Данная команда доступна только для администратора.");
           return;
        }
        terminalService.print("Список пользователей:");
        for (User user : bootstrap.getUserService().findAllUsers(currentSession)) {
            System.out.println(user.getLogin() + " " + user.getRole());
        }
    }
}
