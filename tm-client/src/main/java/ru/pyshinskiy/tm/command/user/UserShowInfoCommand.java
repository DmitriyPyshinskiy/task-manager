package ru.pyshinskiy.tm.command.user;


import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;

@NoArgsConstructor
public final class UserShowInfoCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "show_info";
    }

    @Override
    public String getDescription() {
        return "выводит на экран информацию о профиле.";
    }

    @Override
    public void execute() {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            System.out.println("Защищённая команда. Войдите в систему.");
            return;
        }

        String userId = currentSession.getUserId();
        System.out.println("Информация о профиле: ");
        System.out.println("Тип роли: " + bootstrap.getUserService().findOneUser(currentSession, userId));
        System.out.println("Логин : " + bootstrap.getUserService().findOneUser(currentSession, userId).getLogin());

    }
}
