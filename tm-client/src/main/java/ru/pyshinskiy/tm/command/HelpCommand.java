package ru.pyshinskiy.tm.command;

import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список доступных комманд с описанием.";
    }

    @Override
    public void execute() {
        for (Map.Entry<String, AbstractCommand> pair : bootstrap.getCommandList().entrySet()) {
            System.out.println(pair.getKey() + ": " + pair.getValue().getDescription());
        }
    }
}
