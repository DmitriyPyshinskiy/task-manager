package ru.pyshinskiy.tm.command.user;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.endpoint.User;
import ru.pyshinskiy.tm.endpoint.TypeOfRole;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;
import java.util.UUID;

@NoArgsConstructor
public final class UserRegisterCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "register";
    }

    @Override
    public String getDescription() {
        return "регистрирует нового пользователья в системе.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            if(!(currentSession == null)) {
                terminalService.print("Вы уже зарегестрированны.");
                return;
            }
        } catch(NullPointerException e) {
            terminalService.print("Начало регистрации.");
        }
        terminalService.print("Введите логин:");
        String login;
        while(true) {
            login = bootstrap.getTerminalService().nextLine();
            if(login.equals("exit")) {
                return;
            }
            if(bootstrap.getUserService().doesUserExist(login)) {
                terminalService.print("Пользователь с указанным логином уже существует. Придумайте другой логин.");
                terminalService.print("Или введите \"exit\" для отмены регистрации.");
            }
            else {
                terminalService.print("Логин принят.");
                break;
            }
        }
        terminalService.print("Введите пароль.");
        String rawPassword;
        while(true) {
            rawPassword = bootstrap.getTerminalService().nextLine();
            if(rawPassword.equals("exit")) {
                return;
            }
            if(rawPassword.length() < 8) {
                terminalService.print("Пароль слишком короткий. Он должен состоять не менее чем из восьми символов.");
                terminalService.print("Введите пароль снова или введите \"exit\" для отмены регистрации.");
            }
            else {
                terminalService.print("Пароль принят. Вы успешно зарегистрированны в системе.");
                break;
            }
        }
        String hashPassword = DigestUtils.md5Hex(rawPassword);
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setRole(TypeOfRole.USER);
        user.setLogin(login);
        user.setHashPassword(hashPassword);
        bootstrap.getUserService().persistUser(user);

    }
}
