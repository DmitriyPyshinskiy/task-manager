package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "удаляет указанный проект.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        IProjectEndpoint projectService  = bootstrap.getProjectService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Введите имя проекта, который вы хотите удалить: ");
        String projectName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if(projectService.doesProjectExistById(currentSession, projectName, userId)) {
            for (Project project : projectService.findAllProjectsById(currentSession, userId)) {
                if(project.getName().equals(projectName)) {
                    projectService.detachAllProjectTasks(currentSession, project);
                    projectService.removeProject(currentSession, project.getId());
                }
            }
            terminalService.print("Проект был успешно удалён.");
        }
        else {
            terminalService.print("Проекта с таким именем не существует.");
        }

    }
}
