package ru.pyshinskiy.tm.command.user;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.User;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "edit_profile";
    }

    @Override
    public String getDescription() {
        return "позволяет редактировать профиль.";
    }

    @Override
    public void execute() throws IOException {
        TerminalService terminalService = bootstrap.getTerminalService();
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        User currentUser = bootstrap.getUserService().findOneUser(currentSession, currentSession.getUserId());
        terminalService.print("1. Изменить логин.");
        terminalService.print("Выберете цифру, чтобы внести изменения:");
        String inputUser = terminalService.nextLine();

        if(inputUser.equals("1")) {
            terminalService.print("Введите новый логин:");
            String newLogin = terminalService.nextLine();
            currentUser.setLogin(newLogin);
        }
        else {
            terminalService.print("Вы ввели некорректную цифру.");
        }


    }
}
