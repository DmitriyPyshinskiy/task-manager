package ru.pyshinskiy.tm.command.user;


import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class UserLoginCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "вход в систему.";
    }

    @Override
    public void execute() throws IOException {
        TerminalService terminalService = bootstrap.getTerminalService();
        ISessionEndpoint sessionService = bootstrap.getSessionService();
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        try {
            if(!(currentSession == null)) {
                terminalService.print("Вы уже вошли в систему.");
                return;
            }
        } catch(NullPointerException e) {
            terminalService.print("Начало авторизации.");
        }

        while(bootstrap.getCurrentSessionService().getCurrentSession() == null) {
            terminalService.print("Введите свой логин:");
            final String login = terminalService.nextLine();
            if(login.equals("exit")) {
                break;
            }
            System.out.println("Введите свой пароль:");
            final String password = DigestUtils.md5Hex(terminalService.nextLine());
            if(password.equals("exit")) {
                break;
            }
            try {
                bootstrap.getCurrentSessionService().setCurrentSession(sessionService.createSession(login, password));
                terminalService.print("Вы вошли в систему.");
            } catch (Exception e) {
                terminalService.print("Введены неправильные данные");
                terminalService.print("Для отмены авторизации введите \"exit\"");
            }
        }
    }
}
