package ru.pyshinskiy.tm.command;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "номер сборки проекта.";
    }

    @Override
    public void execute() {
        String buildOfNumber = Manifests.read("Build-Jdk");
        System.out.println(buildOfNumber);

    }
}
