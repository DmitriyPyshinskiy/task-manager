package ru.pyshinskiy.tm.command.admin;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.TypeOfRole;
import ru.pyshinskiy.tm.service.TerminalService;

public final class AdminProjectListUsersCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project_list_users";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список проектов всех пользователей.";
    }

    @Override
    public void execute() {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        if(!currentSession.getUserRole().equals(TypeOfRole.ADMIN)) {
            terminalService.print("Данная команда доступна только для администратора.");
            return;
        }

        if(bootstrap.getProjectService().findAllProjects(currentSession).size() == 0) {
            terminalService.print("Проекты отсутствуют.");
            return;
        }
        terminalService.print("Список проектов:");
        for (Project project : bootstrap.getProjectService().findAllProjects(currentSession)) {
            System.out.println(project.getName());
        }
    }
}
