package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.endpoint.Project;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;
import java.util.*;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "выводит на экран список всех активных проектов.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        IProjectEndpoint projectService = bootstrap.getProjectService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }



        String userId = currentSession.getUserId();
        List<Project> projectList = projectService.findAllProjectsById(currentSession, userId);

        if(projectService.findAllProjectsById(currentSession, userId).size() == 0) {
            terminalService.print("У вас нет активных проектов.");
            return;
        }

        terminalService.print("Выберете, в каком порядке вывести проекты на экран:\n" +
                "1 - В порядке создания в системе.\n" +
                "2 - По дате начала проекта.\n" +
                "3 - По дате окончания проекта.\n" +
                "4 - По статусу готовности.");

        String userInput = terminalService.nextLine();
        switch (userInput) {
            case "1":
                for (Project project : projectService.sortProjectByCreationDate(currentSession, projectList)) {
                    terminalService.print("Проект: " + project.getName() + " создан: " + new Date(project.getCreateTime()).toString());
                }
                break;
            case "2":
                for (Project project : projectService.sortProjectByStartDate(currentSession, projectList)) {
                    terminalService.print("Проект: " + project.getName() + " дата начала: " + Objects.requireNonNull(project.getDateBegin()).toString());
                }
                break;
            case "3":
                for (Project project : projectService.sortProjectByEndDate(currentSession, projectList)) {
                    terminalService.print("Проект: " + project.getName() + " дата окончания: " + Objects.requireNonNull(project.getDateEnd()).toString());
                }
                break;
            case "4":
                for (Project project : projectService.sortProjectByReadinessStatus(currentSession, projectList)) {
                    terminalService.print("Проект: " + project.getName() + " статус готовности: " + project.getStatusReadiness());
                }
                break;
            default:
                 terminalService.print("Вы ввели неправильную цифру.");
                 break;
        }
    }
}
