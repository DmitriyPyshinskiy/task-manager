package ru.pyshinskiy.tm.command.project;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project_clear";
    }

    @Override
    public String getDescription() {
        return "удаляет все проекты.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            System.out.println("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Вы уверены, что хотите удалить все текущие проеткты?");
        terminalService.print("ДА/НЕТ");
        String userAnswer = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if("ДА".equals(userAnswer)) {
            bootstrap.getProjectService().removeAllProjectsById(currentSession, userId);
        }
        terminalService.print("Проекты были успешно удалены.");
    }
}
