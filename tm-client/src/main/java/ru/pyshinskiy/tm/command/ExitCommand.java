package ru.pyshinskiy.tm.command;

import lombok.NoArgsConstructor;


@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "завершает работу программы.";
    }

    @Override
    public void execute() {

    }
}
