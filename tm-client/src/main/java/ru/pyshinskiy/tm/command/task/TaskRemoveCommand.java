package ru.pyshinskiy.tm.command.task;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.endpoint.Task;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task_remove";
    }

    @Override
    public String getDescription() {
        return "удаляет указаную задачу.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Введите имя задачи, которую вы хотите удалить: ");
        String taskName = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if(bootstrap.getTaskService().doesTaskExistById(currentSession, taskName, userId)) {
            for (Task task : bootstrap.getTaskService().findAllTasksById(currentSession, userId)) {
                if(task.getName().equals(taskName)) {
                    bootstrap.getTaskService().removeTask(currentSession, task.getId());
                }
            }
            terminalService.print("Задача была успешно удалена.");
        }
        else {
            terminalService.print("Задачи с таким именем не существует.");
        }
    }
}
