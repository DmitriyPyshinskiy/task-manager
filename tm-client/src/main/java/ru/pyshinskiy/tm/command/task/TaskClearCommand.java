package ru.pyshinskiy.tm.command.task;


import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.endpoint.Session;
import ru.pyshinskiy.tm.service.TerminalService;

import java.io.IOException;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "удаляет все задачи.";
    }

    @Override
    public void execute() throws IOException {
        Session currentSession = bootstrap.getCurrentSessionService().getCurrentSession();
        TerminalService terminalService = bootstrap.getTerminalService();
        try {
            doesCommandSave();
        }catch (NullPointerException e){
            terminalService.print("Защищённая команда. Войдите в систему.");
            return;
        }

        terminalService.print("Вы уверены, что хотите удалить все текущие задачи?");
        terminalService.print("ДА/НЕТ");
        String userAnswer = terminalService.nextLine();
        String userId = currentSession.getUserId();
        if("ДА".equals(userAnswer)) {
            bootstrap.getTaskService().removeAllTasksById(currentSession, userId);
        }
        terminalService.print("Все задачи были успешно удалены.");
    }
}
