package ru.pyshinskiy.tm.command;

import lombok.NoArgsConstructor;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.bootstrap.ServiceLocator;
import ru.pyshinskiy.tm.endpoint.Exception_Exception;
import ru.pyshinskiy.tm.endpoint.Session;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractCommand {
    protected ServiceLocator bootstrap;


    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException, JAXBException, Exception_Exception;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void doesCommandSave() throws NullPointerException {
        String id = bootstrap.getCurrentSessionService().getCurrentSession().getId();
    }

}
