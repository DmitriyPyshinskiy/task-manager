
package ru.pyshinskiy.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.pyshinskiy.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "Exception");
    private final static QName _CreateSession_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "createSession");
    private final static QName _CreateSessionResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "createSessionResponse");
    private final static QName _FindAllSessions_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findAllSessions");
    private final static QName _FindAllSessionsResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findAllSessionsResponse");
    private final static QName _FindOneByUserIdAndSessionId_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findOneByUserIdAndSessionId");
    private final static QName _FindOneByUserIdAndSessionIdResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findOneByUserIdAndSessionIdResponse");
    private final static QName _FindOneSession_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findOneSession");
    private final static QName _FindOneSessionResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "findOneSessionResponse");
    private final static QName _RemoveAllSessions_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "removeAllSessions");
    private final static QName _RemoveAllSessionsResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "removeAllSessionsResponse");
    private final static QName _RemoveSession_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "removeSession");
    private final static QName _RemoveSessionResponse_QNAME = new QName("http://endpoint.tm.pyshinskiy.ru/", "removeSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.pyshinskiy.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CreateSession }
     * 
     */
    public CreateSession createCreateSession() {
        return new CreateSession();
    }

    /**
     * Create an instance of {@link CreateSessionResponse }
     * 
     */
    public CreateSessionResponse createCreateSessionResponse() {
        return new CreateSessionResponse();
    }

    /**
     * Create an instance of {@link FindAllSessions }
     * 
     */
    public FindAllSessions createFindAllSessions() {
        return new FindAllSessions();
    }

    /**
     * Create an instance of {@link FindAllSessionsResponse }
     * 
     */
    public FindAllSessionsResponse createFindAllSessionsResponse() {
        return new FindAllSessionsResponse();
    }

    /**
     * Create an instance of {@link FindOneByUserIdAndSessionId }
     * 
     */
    public FindOneByUserIdAndSessionId createFindOneByUserIdAndSessionId() {
        return new FindOneByUserIdAndSessionId();
    }

    /**
     * Create an instance of {@link FindOneByUserIdAndSessionIdResponse }
     * 
     */
    public FindOneByUserIdAndSessionIdResponse createFindOneByUserIdAndSessionIdResponse() {
        return new FindOneByUserIdAndSessionIdResponse();
    }

    /**
     * Create an instance of {@link FindOneSession }
     * 
     */
    public FindOneSession createFindOneSession() {
        return new FindOneSession();
    }

    /**
     * Create an instance of {@link FindOneSessionResponse }
     * 
     */
    public FindOneSessionResponse createFindOneSessionResponse() {
        return new FindOneSessionResponse();
    }

    /**
     * Create an instance of {@link RemoveAllSessions }
     * 
     */
    public RemoveAllSessions createRemoveAllSessions() {
        return new RemoveAllSessions();
    }

    /**
     * Create an instance of {@link RemoveAllSessionsResponse }
     * 
     */
    public RemoveAllSessionsResponse createRemoveAllSessionsResponse() {
        return new RemoveAllSessionsResponse();
    }

    /**
     * Create an instance of {@link RemoveSession }
     * 
     */
    public RemoveSession createRemoveSession() {
        return new RemoveSession();
    }

    /**
     * Create an instance of {@link RemoveSessionResponse }
     * 
     */
    public RemoveSessionResponse createRemoveSessionResponse() {
        return new RemoveSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "createSession")
    public JAXBElement<CreateSession> createCreateSession(CreateSession value) {
        return new JAXBElement<CreateSession>(_CreateSession_QNAME, CreateSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "createSessionResponse")
    public JAXBElement<CreateSessionResponse> createCreateSessionResponse(CreateSessionResponse value) {
        return new JAXBElement<CreateSessionResponse>(_CreateSessionResponse_QNAME, CreateSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllSessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findAllSessions")
    public JAXBElement<FindAllSessions> createFindAllSessions(FindAllSessions value) {
        return new JAXBElement<FindAllSessions>(_FindAllSessions_QNAME, FindAllSessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllSessionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findAllSessionsResponse")
    public JAXBElement<FindAllSessionsResponse> createFindAllSessionsResponse(FindAllSessionsResponse value) {
        return new JAXBElement<FindAllSessionsResponse>(_FindAllSessionsResponse_QNAME, FindAllSessionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByUserIdAndSessionId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findOneByUserIdAndSessionId")
    public JAXBElement<FindOneByUserIdAndSessionId> createFindOneByUserIdAndSessionId(FindOneByUserIdAndSessionId value) {
        return new JAXBElement<FindOneByUserIdAndSessionId>(_FindOneByUserIdAndSessionId_QNAME, FindOneByUserIdAndSessionId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByUserIdAndSessionIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findOneByUserIdAndSessionIdResponse")
    public JAXBElement<FindOneByUserIdAndSessionIdResponse> createFindOneByUserIdAndSessionIdResponse(FindOneByUserIdAndSessionIdResponse value) {
        return new JAXBElement<FindOneByUserIdAndSessionIdResponse>(_FindOneByUserIdAndSessionIdResponse_QNAME, FindOneByUserIdAndSessionIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findOneSession")
    public JAXBElement<FindOneSession> createFindOneSession(FindOneSession value) {
        return new JAXBElement<FindOneSession>(_FindOneSession_QNAME, FindOneSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "findOneSessionResponse")
    public JAXBElement<FindOneSessionResponse> createFindOneSessionResponse(FindOneSessionResponse value) {
        return new JAXBElement<FindOneSessionResponse>(_FindOneSessionResponse_QNAME, FindOneSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "removeAllSessions")
    public JAXBElement<RemoveAllSessions> createRemoveAllSessions(RemoveAllSessions value) {
        return new JAXBElement<RemoveAllSessions>(_RemoveAllSessions_QNAME, RemoveAllSessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "removeAllSessionsResponse")
    public JAXBElement<RemoveAllSessionsResponse> createRemoveAllSessionsResponse(RemoveAllSessionsResponse value) {
        return new JAXBElement<RemoveAllSessionsResponse>(_RemoveAllSessionsResponse_QNAME, RemoveAllSessionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "removeSession")
    public JAXBElement<RemoveSession> createRemoveSession(RemoveSession value) {
        return new JAXBElement<RemoveSession>(_RemoveSession_QNAME, RemoveSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.pyshinskiy.ru/", name = "removeSessionResponse")
    public JAXBElement<RemoveSessionResponse> createRemoveSessionResponse(RemoveSessionResponse value) {
        return new JAXBElement<RemoveSessionResponse>(_RemoveSessionResponse_QNAME, RemoveSessionResponse.class, null, value);
    }

}
