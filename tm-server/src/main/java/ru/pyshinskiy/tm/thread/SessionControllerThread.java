package ru.pyshinskiy.tm.thread;

import lombok.AllArgsConstructor;
import ru.pyshinskiy.tm.constant.SecurityConst;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.service.ISessionService;

import java.util.Date;

@AllArgsConstructor
public class SessionControllerThread implements Runnable {
    ISessionService sessionService;

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(5555);
                for (Session session : sessionService.findAll()) {
                    long currentTime = new Date().getTime();
                    long result = currentTime - session.getCreateTime().getTime();

                    if(result > SecurityConst.TIME_LIVE_SESSION) {
                        sessionService.removeOneById(session.getId());
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
