package ru.pyshinskiy.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

public interface SessionRepositoryMapper {

    @Select("SELECT * FROM app_session")
    List<Session> findAll();

    @Select("SELECT * FROM app_session WHERE id = #{id}")
    Session findOneById(@Nullable String id);

    @Select("SELECT * FROM app_session WHERE userId = #{userId}")
    Session findOneByUserId(@Nullable String userId);

    @Select("INSERT INTO app_session (id, userId, userRole, createTime, signature)" +
            " VALUES (#{id}, #{userId}, #{userRole}, #{createTime}, #{signature})")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    Session persist(@Nullable Session entity);

    @Update("UPDATE app_session SET userId = #{userId}, userRole = #{userRole}" +
            "createTime = #{createTime}, signature = #{signature} WHERE id = #{id}")
    void merge(@Nullable String id, @Nullable Session entity);

    @Delete("DELETE FROM app_session")
    void removeAll();

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void removeOneById(@Nullable String id);
}
