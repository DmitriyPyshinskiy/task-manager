package ru.pyshinskiy.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface TaskRepositoryMapper {

    @Select("SELECT * FROM app_task")
    List<Task> findAll();

    @Select("SELECT * FROM app_task WHERE userId = #{userId}")
    List<Task> findAllByUserId(@Nullable String userId);

    @Select("SELECT * FROM app_task WHERE id = #{id}")
    Task findOneById(@Nullable String id);

    @Select("SELECT * FROM app_task WHERE name = #{taskName} AND userId = #{userId}")
    Task findOneByNameAndUserId(@Nullable String taskName, String userId);

    @Select("INSERT INTO app_task (id, name, description, statusReadiness, createTime, dateBegin, dateEnd, userId)" +
            "VALUES (#{id}, #{name}, #{description}, #{statusReadiness}, #{createTime}, #{dateBegin}, #{dateEnd}, #{userId})")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    Task persist(Task entity);

    @Update("UPDATE app_task " +
            "SET name = #{name}, description = #{description}, statusReadiness = #{statusReadiness}," +
            "createTime = #{createTime}, dateBegin = #{dateBegin}, dateEnd = #{dateEnd}, userId = #{userId})" +
            "WHERE id = #{id}")
    void merge(String id, @Nullable Task entity);

    @Delete("DELETE FROM app_task")
    void removeAll();

    @Delete("DELETE FROM app_task WHERE userId = #{userId}")
    void removeAllByUserId(@Nullable String userId);

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    void removeOneById(@Nullable String id);

    @Select("SELECT * FROM app_task WHERE name LIKE %#{namePart}%")
    Task findOneByNamePart(@Nullable String namePart);

    @Select("SELECT * FROM app_task WHERE description LIKE %#{descriptionPart}%")
    Task findOneByDescriptionPart(@Nullable String descriptionPart);
}
