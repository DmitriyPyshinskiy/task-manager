package ru.pyshinskiy.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface ProjectRepositoryMapper {

    @Select("SELECT * FROM app_project")
    List<Project> findAll();

    @Select("SELECT * FROM app_project WHERE userId = #{userId}")
    List<Project> findAllByUserId(@Nullable String userId);

    @Select("SELECT * FROM app_project WHERE id = #{id}")
    Project findOneById(@Nullable String id);

    @Select("SELECT * FROM app_project WHERE name = #{projectName} AND userId = #{userId}")
    Project findOneByNameAndUserId(@Nullable String projectName, String userId);

    @Select("INSERT INTO app_project (id, name, description, statusReadiness, createTime, dateBegin, dateEnd, userId)" +
            "VALUES (#{id}, #{name}, #{description}, #{statusReadiness}, #{createTime}, #{dateBegin}, #{dateEnd}, #{userId})")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    Project persist(Project entity);

    @Update("UPDATE app_project " +
            "SET id = #{id}, #{name}, description = #{description}, statusReadiness = #{statusReadiness}," +
            "createTime = #{createTime}, dateBegin = #{dateBegin}, dateEnd = #{dateEnd}, userId = #{userId})" +
            "WHERE id = #{oldId}")
    void merge(@Param(value="oldId") String id, @Nullable Project entity);

    @Delete("DELETE FROM app_project")
    void removeAll();

    @Delete("DELETE FROM app_project WHERE userId = #{userId}")
    void removeAllByUserId(@Nullable String userId);

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    void removeOneById(@Nullable String id);

    @Select("SELECT * FROM app_project WHERE app_project.name LIKE '%#{namePart}%'")
    Project findOneByNamePart(@Nullable String namePart);

    @Select("SELECT * FROM app_project WHERE app_project.description LIKE '%#{descriptionPart}%'")
    Project findOneByDescriptionPart(@Nullable String descriptionPart);
}
