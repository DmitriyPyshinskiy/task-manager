package ru.pyshinskiy.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface UserRepositoryMapper {

    @Select("SELECT * FROM app_user")
    List<User> findAll();

    @Select("SELECT * FROM app_user WHERE id = #{id}")
    User findOneById(@Nullable String id);

    @Select("INSERT INTO app_user (id, role, login, hashPassword)" +
            "VALUES id = #{id}, role = #{role}, login = #{login}, hashPassword = #{hashPassword}")
    @Options(flushCache = Options.FlushCachePolicy.TRUE)
    User persist(@Nullable User entity);

    @Update("UPDATE app_user SET role = #{role}, login = #{login}, hashPassword = #{hashPassword}" +
            "WHERE id = #{id}")
    void merge(@Nullable String id, @Nullable User entity);

    @Delete("DELETE FROM app_user")
    void removeAll();

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void removeOneById(@Nullable String id);

}
