package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumeration.TypeOfRole;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @Nullable
    private String userId;
    @Nullable
    private TypeOfRole userRole;
    private Date createTime = new Date(System.currentTimeMillis());
    @Nullable
    private String signature;

    public Session(@Nullable String userId, @Nullable TypeOfRole userRole, @Nullable String signature) {
        this.userId = userId;
        this.userRole = userRole;
        this.signature = signature;
    }
}
