package ru.pyshinskiy.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumeration.TypeOfRole;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity implements Serializable {

    @Nullable
    private TypeOfRole role;
    @Nullable
    private String login;
    @Nullable
    private String hashPassword;

    public User(@Nullable TypeOfRole role, @Nullable String login, @Nullable String hashPassword) {
        this.role = role;
        this.login = login;
        this.hashPassword = hashPassword;
    }
}
