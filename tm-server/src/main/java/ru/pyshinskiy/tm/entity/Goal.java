package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumeration.StatusReadiness;

import java.util.Date;

@Getter
@Setter
public abstract class Goal extends AbstractEntity {

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @Nullable
    protected String userId;

    @NotNull
    protected StatusReadiness statusReadiness = StatusReadiness.PLANNED;

    protected long createTime = new Date().getTime();

    @Nullable
    protected Date dateBegin;

    @Nullable
    protected Date dateEnd;
}
