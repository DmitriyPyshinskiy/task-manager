package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends Goal implements Serializable {

    @Nullable
    private String projectId;

    public Task(@Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }
}
