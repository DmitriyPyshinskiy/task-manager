package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends Goal implements Serializable {

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    public Project(@Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd) {
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }
}
