package ru.pyshinskiy.tm.bootstrap;

import ru.pyshinskiy.tm.service.IProjectService;
import ru.pyshinskiy.tm.service.ITaskService;
import ru.pyshinskiy.tm.service.IUserService;

import java.io.BufferedReader;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    BufferedReader getReader();

}
