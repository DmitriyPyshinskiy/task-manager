package ru.pyshinskiy.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.endpoint.impl.*;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumeration.TypeOfRole;
import ru.pyshinskiy.tm.repository.ProjectRepositoryMapper;
import ru.pyshinskiy.tm.repository.SessionRepositoryMapper;
import ru.pyshinskiy.tm.repository.TaskRepositoryMapper;
import ru.pyshinskiy.tm.repository.UserRepositoryMapper;
import ru.pyshinskiy.tm.service.*;
import ru.pyshinskiy.tm.service.impl.*;
import ru.pyshinskiy.tm.util.db.JDBCConnection;

import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    @Nullable
    private final Connection connection = JDBCConnection.getStatement();
    @NotNull
    private final IProjectService projectService = new ProjectService();
    @NotNull
    private final ITaskService taskService = new TaskService();
    @NotNull
    private final IUserService userService = new UserService();
    @NotNull
    private final ISessionService sessionService = new SessionService(userService);


    public void init() {
        createTwoUsers();
        Endpoint.publish("http://localhost:8080/ws/projectEndpoint?wsdl", new ProjectEndpoint(projectService, sessionService));
        Endpoint.publish("http://localhost:8080/ws/taskEndpoint?wsdl", new TaskEndpoint(taskService, sessionService));
        Endpoint.publish("http://localhost:8080/ws/userEndpoint?wsdl", new UserEndpoint(userService, sessionService));
        Endpoint.publish("http://localhost:8080/ws/sessionEndpoint?wsdl", new SessionEndpoint(sessionService));
        System.out.println("Server is started");
    }

    private void createTwoUsers() {

        if(!userService.doesUserExistByLogin("dimasik")) {
            User dimasik = new User(TypeOfRole.ADMIN, "dimasik", DigestUtils.md5Hex("zcj,frf,"));
            userService.persist(dimasik);
        }
        if(!userService.doesUserExistByLogin("pavlosik")) {
            User pavlosik = new User(TypeOfRole.USER, "pavlosik", DigestUtils.md5Hex("pavlosik"));
            userService.persist(pavlosik);
        }
    }
}
