package ru.pyshinskiy.tm.util.sort;

import ru.pyshinskiy.tm.entity.Goal;

import java.util.Comparator;
import java.util.List;

public final class SortMethods {

    public static <E extends Goal> List<E> sortByCreationDate(List<E> list) {
        list.sort((goal, t1) -> Math.toIntExact(goal.getCreateTime() - t1.getCreateTime()));
        return list;
    }

    public static <E extends Goal> List<E> sortByStartDate(List<E> list) {
        list.sort((goal, t1) -> {
            assert goal.getDateBegin() != null;
            assert t1.getDateBegin() != null;
            return (int) (goal.getDateBegin().getTime() - t1.getDateBegin().getTime());
        });
        return list;
    }

    public static <E extends Goal> List<E> sortByEndDate(List<E> list) {
        list.sort((goal, t1) -> {
            assert goal.getDateEnd() != null;
            assert t1.getDateEnd() != null;
            return Math.toIntExact(goal.getDateEnd().getTime() - t1.getDateEnd().getTime());
        });
        return list;
    }

    public static <E extends Goal> List<E> sortByReadinessStatus(List<E> list) {
        list.sort(Comparator.comparing(Goal::getStatusReadiness));
        return list;
    }
}
