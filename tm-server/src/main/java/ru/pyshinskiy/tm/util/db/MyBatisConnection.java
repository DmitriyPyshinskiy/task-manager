package ru.pyshinskiy.tm.util.db;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import ru.pyshinskiy.tm.repository.ProjectRepositoryMapper;
import ru.pyshinskiy.tm.repository.SessionRepositoryMapper;
import ru.pyshinskiy.tm.repository.TaskRepositoryMapper;
import ru.pyshinskiy.tm.repository.UserRepositoryMapper;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class MyBatisConnection {

    public static SqlSessionFactory getSqlSessionFactoryFormJava() {
        Configuration configuration = null;
        try {
            Properties property = new Properties();
            property.load(new FileInputStream("/home/dima/IdeaProjects/Task-manager/tm-server/jdbc.properties"));
            String driver = property.getProperty("driver");
            String url = property.getProperty("url");
            String username = property.getProperty("username");
            String password = property.getProperty("password");

            Class.forName(driver);

            DataSource dataSource = new PooledDataSource(driver, url, username, password);

            Environment environment = new Environment("development", new JdbcTransactionFactory(), dataSource);

            configuration = new Configuration(environment);
            configuration.addMapper(ProjectRepositoryMapper.class);
            configuration.addMapper(TaskRepositoryMapper.class);
            configuration.addMapper(SessionRepositoryMapper.class);
            configuration.addMapper(UserRepositoryMapper.class);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
