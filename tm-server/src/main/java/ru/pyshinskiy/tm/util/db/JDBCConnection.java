package ru.pyshinskiy.tm.util.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class JDBCConnection {


    public static Connection getStatement() {
        try {
            Properties property = new Properties();
            property.load(new FileInputStream("/home/dima/IdeaProjects/Task-manager/tm-server/jdbc.properties"));
            String driver = property.getProperty("driver");
            String url = property.getProperty("url");
            String username = property.getProperty("username");
            String password = property.getProperty("password");

            Class.forName(driver);
            return DriverManager.getConnection(url, username, password);


        } catch (ClassNotFoundException | SQLException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
