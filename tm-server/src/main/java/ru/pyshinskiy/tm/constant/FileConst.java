package ru.pyshinskiy.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FileConst {
    @NotNull
    public static final String BINARY_FILE = "/home/dima/IdeaProjects/Task-manager/binaryData.txt";
    @NotNull
    public static final String XML_FILE_JAXB = "/home/dima/IdeaProjects/Task-manager/dataInXmlJaxb.xml";
    @NotNull
    public static final String JSON_FILE_JAXB = "/home/dima/IdeaProjects/Task-manager/dataInJsonJaxb.json";
    @NotNull
    public static final String XML_FILE_FASTER = "/home/dima/IdeaProjects/Task-manager/dataInXmlFaster.xml";
    @NotNull
    public static final String JSON_FILE_FASTER = "/home/dima/IdeaProjects/Task-manager/dataInJsonFaster.json";


}
