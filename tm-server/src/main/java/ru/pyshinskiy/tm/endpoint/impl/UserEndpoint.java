package ru.pyshinskiy.tm.endpoint.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.constant.SecurityConst;
import ru.pyshinskiy.tm.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.exception.InvalidSessionException;
import ru.pyshinskiy.tm.service.ISessionService;
import ru.pyshinskiy.tm.service.IUserService;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "ru.pyshinskiy.tm.endpoint.IUserEndpoint")
@NoArgsConstructor
@AllArgsConstructor
public class UserEndpoint implements IUserEndpoint {
    private IUserService userService;
    private ISessionService sessionService;


    @WebMethod
    @Override
    public boolean doesUserExist(@Nullable String userLogin) {
        return userService.doesUserExistByLogin(userLogin);
    }

    @WebMethod
    @Override
    public List<User> findAllUsers(Session session) {
        validateSession(session);
        return userService.findAll();
    }

    @WebMethod
    @Override
    public User findOneUser(Session session, @Nullable String id) {
        validateSession(session);
        return userService.findOneById(id);
    }

    @WebMethod
    @Override
    public void persistUser(@Nullable User entity) {
        userService.persist(entity);
    }

    @WebMethod
    @Override
    public void mergeUser(Session session, @Nullable String id, @Nullable User entity) {
        validateSession(session);
        userService.merge(id, entity);
    }

    @WebMethod
    @Override
    public void removeUser(Session session, @Nullable String id) {
        validateSession(session);
        userService.removeOneById(id);
    }

    @WebMethod
    @Override
    public void removeAllUsers(Session session) {
        validateSession(session);
        userService.removeAll();
    }

    private void validateSession(Session userSession) {
        userSession.setSignature(null);
        Session backSession  = sessionService.findOneById(userSession.getId());
        String userSignature = SignatureUtil.sign(userSession, SecurityConst.SALT, SecurityConst.CYCLE);
        if(Objects.requireNonNull(userSignature).equals(backSession.getSignature())) {
            return;
        }
        throw new InvalidSessionException();
    }
}
