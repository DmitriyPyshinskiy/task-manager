package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    List<Project> findAllProjectsById(Session session, @Nullable String userId);

    @WebMethod
    Project findOneProjectByNameAndId(Session session, @Nullable String projectName, @Nullable String userId);

    @WebMethod
    void removeAllProjectsById(Session session, @Nullable String userId);

    @WebMethod
    List<Task> returnProjectListTasks(Session session, @Nullable Project project);

    @WebMethod
    void attachProjectTask(Session session, @Nullable Project project, @Nullable Task task);

    @WebMethod
    void detachProjectTask(Session session, @Nullable Project project, @Nullable Task task);

    @WebMethod
    void detachAllProjectTasks(Session session, @Nullable Project project);

    @WebMethod
    boolean doesProjectExist(Session session, @Nullable String projectName);

    @WebMethod
    boolean doesProjectExistById(Session session, @Nullable String projectName, @Nullable String userId);

    @WebMethod
    Project findOneProjectByNamePart(Session session, String string);

    @WebMethod
    Project findOneProjectByDescriptionPart(Session session, String string);

    @WebMethod
    List<Project> sortProjectByCreationDate(Session session, List<Project> projectList);

    @WebMethod
    List<Project> sortProjectByStartDate(Session session, List<Project> projectList);

    @WebMethod
    List<Project> sortProjectByEndDate(Session session, List<Project> projectList);

    @WebMethod
    List<Project> sortProjectByReadinessStatus(Session session, List<Project> projectList);

    @WebMethod
    List<Project> findAllProjects(Session session);

    @WebMethod
    Project findOneProject(Session session, @Nullable String id);

    @WebMethod
    void persistProject(Session session, @Nullable Project entity);

    @WebMethod
    void mergeProject(Session session, @Nullable String id, @Nullable Project entity);

    @WebMethod
    void removeProject(Session session, @Nullable String id);

    @WebMethod
    void removeAllProjects(Session session);
}

