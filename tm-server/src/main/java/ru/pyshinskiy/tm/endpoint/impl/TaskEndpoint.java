package ru.pyshinskiy.tm.endpoint.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.constant.SecurityConst;
import ru.pyshinskiy.tm.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.exception.InvalidSessionException;
import ru.pyshinskiy.tm.service.ISessionService;
import ru.pyshinskiy.tm.service.ITaskService;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "ru.pyshinskiy.tm.endpoint.ITaskEndpoint")
@NoArgsConstructor
@AllArgsConstructor
public class TaskEndpoint implements ITaskEndpoint {
    private ITaskService taskService;
    private ISessionService sessionService;


    @WebMethod
    @Override
    public List<Task> findAllTasksById(Session session, @Nullable String userId) {
        validateSession(session);
        return taskService.findAllByUserId(userId);
    }

    @WebMethod
    @Override
    public Task findOneTaskByNameAndId(Session session, @Nullable String taskName, @Nullable String userId) {
        validateSession(session);
        return taskService.findOneByNameAndUserId(taskName, userId);
    }

    @WebMethod
    @Override
    public void removeAllTasksById(Session session, @Nullable String userId) {
        validateSession(session);
        taskService.removeAllByUserId(userId);
    }

    @WebMethod
    @Override
    public boolean doesTaskExist(Session session, @Nullable String taskName) {
        validateSession(session);
        return taskService.doesTaskExistByName(taskName);
    }

    @WebMethod
    @Override
    public boolean doesTaskExistById(Session session, @Nullable String taskName, @Nullable String userId) {
        validateSession(session);
        return taskService.doesTaskExistByNameAndUserId(taskName, userId);
    }

    @WebMethod
    @Override
    public Task findOneTaskByNamePart(Session session, String string) {
        validateSession(session);
        return taskService.findOneByNamePart(string);
    }

    @WebMethod
    @Override
    public Task findOneTaskByDescriptionPart(Session session, String string) {
        validateSession(session);
        return taskService.findOneByDescriptionPart(string);
    }

    @WebMethod
    @Override
    public List<Task> sortTaskByCreationDate(Session session, List<Task> taskList) {
        validateSession(session);
        return taskService.sortByCreationDate(taskList);
    }

    @WebMethod
    @Override
    public List<Task> sortTaskByStartDate(Session session, List<Task> taskList) {
        validateSession(session);
        return taskService.sortByStartDate(taskList);
    }

    @WebMethod
    @Override
    public List<Task> sortTaskByEndDate(Session session, List<Task> taskList) {
        validateSession(session);
        return taskService.sortByEndDate(taskList);
    }

    @WebMethod
    @Override
    public List<Task> sortTaskByStatusReadiness(Session session, List<Task> taskList) {
        validateSession(session);
        return taskService.sortByStatusReadiness(taskList);
    }

    @WebMethod
    @Override
    public List<Task> findAllTasks(Session session) {
        validateSession(session);
        return taskService.findAll();
    }

    @WebMethod
    @Override
    public Task findOneTask(Session session, @Nullable String id) {
        validateSession(session);
        return taskService.findOneById(id);
    }

    @WebMethod
    @Override
    public void persistTask(Session session, @Nullable Task entity) {
        validateSession(session);
        taskService.persist(entity);
    }

    @WebMethod
    @Override
    public void mergeTask(Session session, @Nullable String id, @Nullable Task entity) {
        validateSession(session);
        taskService.merge(id, entity);
    }

    @WebMethod
    @Override
    public void removeTask(Session session, @Nullable String id) {
        validateSession(session);
        taskService.removeOneById(id);
    }

    @WebMethod
    @Override
    public void removeAllTasks(Session session) {
        validateSession(session);
        taskService.removeAll();
    }

    private void validateSession(Session userSession) {
        userSession.setSignature(null);
        Session backSession  = sessionService.findOneById(userSession.getId());
        String userSignature = SignatureUtil.sign(userSession, SecurityConst.SALT, SecurityConst.CYCLE);
        if(Objects.requireNonNull(userSignature).equals(backSession.getSignature())) {
            return;
        }
        throw new InvalidSessionException();
    }
}
