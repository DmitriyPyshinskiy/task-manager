package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Goal;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    List<Task> findAllTasksById(Session session, @Nullable String userId);

    @WebMethod
    Task findOneTaskByNameAndId(Session session, @Nullable String taskName, @Nullable String userId);

    @WebMethod
    void removeAllTasksById(Session session, @Nullable String userId);

    @WebMethod
    boolean doesTaskExist(Session session, @Nullable String taskName);

    @WebMethod
    boolean doesTaskExistById(Session session, @Nullable String taskName, @Nullable String userId);

    @WebMethod
    Task findOneTaskByNamePart(Session session, String string);

    @WebMethod
    Task findOneTaskByDescriptionPart(Session session, String string);

    @WebMethod
    List<Task> sortTaskByCreationDate(Session session, List<Task> taskList);

    @WebMethod
    List<Task> sortTaskByStartDate(Session session, List<Task> taskList);

    @WebMethod
    List<Task> sortTaskByEndDate(Session session, List<Task> taskList);

    @WebMethod
    List<Task> sortTaskByStatusReadiness(Session session, List<Task> taskList);

    List<Task> findAllTasks(Session session);

    Task findOneTask(Session session, @Nullable String id);

    void persistTask(Session session, @Nullable Task entity);

    void mergeTask(Session session, @Nullable String id, @Nullable Task entity);

    void removeTask(Session session, @Nullable String id);

    void removeAllTasks(Session session);
}
