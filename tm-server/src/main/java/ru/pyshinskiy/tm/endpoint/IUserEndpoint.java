package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    boolean doesUserExist(@Nullable String userLogin);

    @WebMethod
    List<User> findAllUsers(Session session);

    @WebMethod
    User findOneUser(Session session, @Nullable String id);

    @WebMethod
    void persistUser(@Nullable User entity);

    @WebMethod
    void mergeUser(Session session, @Nullable String id, @Nullable User entity);

    @WebMethod
    void removeUser(Session session, @Nullable String id);

    @WebMethod
    void removeAllUsers(Session session);
}
