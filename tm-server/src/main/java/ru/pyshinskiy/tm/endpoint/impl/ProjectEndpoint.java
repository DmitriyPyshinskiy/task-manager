package ru.pyshinskiy.tm.endpoint.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.constant.SecurityConst;
import ru.pyshinskiy.tm.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.exception.InvalidSessionException;
import ru.pyshinskiy.tm.service.IProjectService;
import ru.pyshinskiy.tm.service.ISessionService;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "ru.pyshinskiy.tm.endpoint.IProjectEndpoint")
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEndpoint implements IProjectEndpoint {
    private IProjectService projectService;
    private ISessionService sessionService;


    @Override
    @WebMethod
    public List<Project> findAllProjectsById(Session session, @Nullable String userId) {
        validateSession(session);
        return projectService.findAllByUserId(userId);
    }

    @Override
    @WebMethod
    public Project findOneProjectByNameAndId(Session session, @Nullable String projectName, @Nullable String userId) {
        validateSession(session);
        return projectService.findOneByNameAndUserId(projectName, userId);
    }

    @Override
    @WebMethod
    public void removeAllProjectsById(Session session, @Nullable String userId) {
        validateSession(session);
        projectService.removeAllByUserId(userId);
    }

    @Override
    @WebMethod
    public List<Task> returnProjectListTasks(Session session, @Nullable Project project) {
        validateSession(session);
        return projectService.returnListTasks(project);
    }

    @Override
    @WebMethod
    public void attachProjectTask(Session session, @Nullable Project project, @Nullable Task task) {
        validateSession(session);
        projectService.attachTask(project, task);
    }

    @Override
    @WebMethod
    public void detachProjectTask(Session session, @Nullable Project project, @Nullable Task task) {
        validateSession(session);
        projectService.detachTask(project, task);
    }

    @Override
    @WebMethod
    public void detachAllProjectTasks(Session session, @Nullable Project project) {
        validateSession(session);
        projectService.detachAllTasks(project);
    }

    @WebMethod
    public boolean doesProjectExist(Session session, @Nullable String projectName) {
        validateSession(session);
        return projectService.doesProjectExistByName(projectName);
    }

    @Override
    @WebMethod
    public boolean doesProjectExistById(Session session, @Nullable String projectName, @Nullable String userId) {
        validateSession(session);
        return projectService.doesProjectExistByNameAndUserId(projectName, userId);
    }

    @WebMethod
    public Project findOneProjectByNamePart(Session session, String string) {
        validateSession(session);
        return projectService.findOneByNamePart(string);
    }

    @Override
    @WebMethod
    public Project findOneProjectByDescriptionPart(Session session, String string) {
        validateSession(session);
        return projectService.findOneByDescriptionPart(string);
    }

    @Override
    @WebMethod
    public List<Project> sortProjectByCreationDate(Session session, List<Project> projectList) {
        validateSession(session);
        return projectService.sortByCreationDate(projectList);
    }

    @Override
    @WebMethod
    public List<Project> sortProjectByStartDate(Session session, List<Project> projectList) {
        validateSession(session);
        return projectService.sortByStartDate(projectList);
    }

    @Override
    @WebMethod
    public List<Project> sortProjectByEndDate(Session session, List<Project> projectList) {
        validateSession(session);
        return projectService.sortByEndDate(projectList);
    }

    @Override
    @WebMethod
    public List<Project> sortProjectByReadinessStatus(Session session, List<Project> projectList) {
        validateSession(session);
        return projectService.sortByReadinessStatus(projectList);
    }

    @WebMethod
    @Override
    public List<Project> findAllProjects(Session session) {
        validateSession(session);
        return projectService.findAll();
    }

    @WebMethod
    @Override
    public Project findOneProject(Session session, @Nullable String id) {
        validateSession(session);
        return projectService.findOneById(id);
    }

    @WebMethod
    @Override
    public void persistProject(Session session, @Nullable Project entity) {
        validateSession(session);
        projectService.persist(entity);
    }

    @WebMethod
    @Override
    public void mergeProject(Session session, @Nullable String id, @Nullable Project entity) {
        validateSession(session);
        projectService.merge(id, entity);
    }

    @WebMethod
    @Override
    public void removeProject(Session session, @Nullable String id) {
        validateSession(session);
        projectService.removeOneById(id);
    }

    @WebMethod
    @Override
    public void removeAllProjects(Session session) {
        validateSession(session);
        projectService.removeAll();
    }

    private void validateSession(Session userSession) {
        userSession.setSignature(null);
        Session backSession  = sessionService.findOneById(userSession.getId());
        String userSignature = SignatureUtil.sign(userSession, SecurityConst.SALT, SecurityConst.CYCLE);
        if(Objects.requireNonNull(userSignature).equals(backSession.getSignature())) {
            return;
        }
        throw new InvalidSessionException();
    }
}
