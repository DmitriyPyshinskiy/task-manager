package ru.pyshinskiy.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    Session findOneByUserIdAndSessionId();

    @WebMethod
    List<Session> findAllSessions();

    @WebMethod
    Session findOneSession(@Nullable String userId);

    @WebMethod
    Session createSession(@Nullable String login, @Nullable String password) throws Exception;

    @WebMethod
    void removeSession(@Nullable String id);

    @WebMethod
    void removeAllSessions();
}
