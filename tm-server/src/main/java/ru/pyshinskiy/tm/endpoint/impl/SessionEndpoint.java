package ru.pyshinskiy.tm.endpoint.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.service.ISessionService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.pyshinskiy.tm.endpoint.ISessionEndpoint")
@NoArgsConstructor
@AllArgsConstructor
public class SessionEndpoint implements ISessionEndpoint {
    private ISessionService sessionService;



    @WebMethod
    @Override
    public Session findOneByUserIdAndSessionId() {
        return sessionService.findOneByUserIdAndSessionId(null, null);
    }

    @WebMethod
    @Override
    public List<Session> findAllSessions() {
        return sessionService.findAll();
    }

    @WebMethod
    @Override
    public Session findOneSession(@Nullable String id) {
        return sessionService.findOneById(id);
    }

    @WebMethod
    @Override
    public Session createSession(@Nullable String login, @Nullable String password) throws Exception {
        return sessionService.persist(login, password);
    }

    @WebMethod
    @Override
    public void removeSession(@Nullable String id) {
        sessionService.removeOneById(id);
    }

    @WebMethod
    @Override
    public void removeAllSessions() {
        sessionService.removeAll();
    }
}
