package ru.pyshinskiy.tm.enumeration;


public enum StatusReadiness {

    PLANNED("Запланированно"),
    PROCESS("В процессе"),
    DONE("Завершено");

    String displayName;

    StatusReadiness(String displayName) {
        this.displayName = displayName;
    }
}