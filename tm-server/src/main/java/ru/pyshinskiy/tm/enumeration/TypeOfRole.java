package ru.pyshinskiy.tm.enumeration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum TypeOfRole {

    @NotNull
    ADMIN("Admin"),
    @NotNull
    USER("User");

    @Nullable
    String displayName;

    TypeOfRole(@Nullable String displayName) {
        this.displayName = displayName;
    }
}