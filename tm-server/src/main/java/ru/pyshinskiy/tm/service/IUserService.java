package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserService {


    List<User> findAll();

    User findOneById(@Nullable String id);

    void persist(@Nullable User entity);

    void merge(@Nullable String id, @Nullable User entity);

    void removeAll();

    void removeOneById(@Nullable String id);

    boolean doesUserExistByLogin(@Nullable String userLogin);
}
