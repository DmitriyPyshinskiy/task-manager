package ru.pyshinskiy.tm.service.impl;


import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.exception.ProjectNotFoundException;
import ru.pyshinskiy.tm.exception.TaskNotFoundException;
import ru.pyshinskiy.tm.repository.ProjectRepositoryMapper;
import ru.pyshinskiy.tm.service.IProjectService;
import ru.pyshinskiy.tm.util.db.MyBatisConnection;
import ru.pyshinskiy.tm.util.sort.SortMethods;

import java.util.List;
import java.util.Objects;


public final class ProjectService implements IProjectService {

    @Override
    public List<Project> findAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            return mapper.findAll();
        }
    }

    @Override
    public List<Project> findAllByUserId(@Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            return sqlSession.getMapper(ProjectRepositoryMapper.class).findAllByUserId(userId);
        }
    }

    @Override
    public Project findOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            return mapper.findOneById(id);
        }
    }

    public Project findOneByNameAndUserId(@Nullable String projectName, @Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            return mapper.findOneByNameAndUserId(projectName, userId);
        }
    }

    @Override
    public void persist(@Nullable Project entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            mapper.persist(entity);
            sqlSession.commit();
        }
    }

    @Override
    public void merge(@Nullable String id, @Nullable Project entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            mapper.merge(id, entity);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            mapper.removeAll();
            sqlSession.commit();
        }
    }


    public void removeAllByUserId(@Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            mapper.removeAllByUserId(userId);
            sqlSession.commit();
        }
    }

    public void removeOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            mapper.removeOneById(id);
            sqlSession.commit();
        }
    }

    public List<Task> returnListTasks(@Nullable Project project) {
        if (project != null) {
            return project.getTasks();
        } else {
            throw new ProjectNotFoundException();
        }
    }

    public void attachTask(@Nullable Project project, @Nullable Task task) {
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        else if(task == null) {
            throw new TaskNotFoundException();
        }
        task.setProjectId(project.getId());
        project.getTasks().add(task);
    }

    public void detachTask(@Nullable Project project, @Nullable Task task) {
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        else if(task == null) {
            throw new TaskNotFoundException();
        }
        project.getTasks().remove(task);
    }

    public void detachAllTasks(@Nullable Project project) {
        if (project != null) {
            project.getTasks().clear();
        } else {
            throw new ProjectNotFoundException();
        }
    }

    public boolean doesProjectExistByName(@Nullable String projectName) {
        return findAll().stream()
                .anyMatch(project -> Objects.requireNonNull(project.getName()).equals(projectName));
    }

    public boolean doesProjectExistByNameAndUserId(@Nullable String projectName, @Nullable String userId) {
        return findAllByUserId(userId).stream()
                .anyMatch(project -> Objects.requireNonNull(project.getName()).equals(projectName));

    }

    @Override
    public @Nullable Project findOneByNamePart(String string) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            return mapper.findOneByNamePart(string);
        }
    }

    @Override
    public @Nullable Project findOneByDescriptionPart(String string) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            ProjectRepositoryMapper mapper = sqlSession.getMapper(ProjectRepositoryMapper.class);
            return mapper.findOneByDescriptionPart(string);
        }
    }



    public List<Project> sortByCreationDate(List<Project> projectList) {
        return SortMethods.sortByCreationDate(projectList);
    }

    public List<Project> sortByStartDate(List<Project> projectList) {
        return SortMethods.sortByStartDate(projectList);
    }

    public List<Project> sortByEndDate(List<Project> projectList) {
        return SortMethods.sortByEndDate(projectList);
    }

    public List<Project> sortByReadinessStatus(List<Project> projectList) {
        return SortMethods.sortByReadinessStatus(projectList);
    }
}
