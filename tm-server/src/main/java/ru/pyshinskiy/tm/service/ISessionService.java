package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Session;

import java.util.List;

public interface ISessionService {

    List<Session> findAll();

    Session findOneById(String id);

    Session findOneByUserId(String userId);

    Session findOneByUserIdAndSessionId(String userId, String sessionId);

    Session persist(String login, String password) throws Exception;

    void removeAll();

    void removeOneById(@Nullable String id);

    boolean doesSessionExistByUserId(String userId);
}
