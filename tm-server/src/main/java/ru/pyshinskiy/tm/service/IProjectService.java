package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAllByUserId(@Nullable String userId);

    Project findOneById(@Nullable String id);

    Project findOneByNameAndUserId(@Nullable String projectName, @Nullable String userId);

    void persist(@Nullable Project entity);

    void merge(String id, Project entity);

    void removeAll();

    void removeAllByUserId(@Nullable String userId);

    void removeOneById(@Nullable String id);

    List<Task> returnListTasks(@Nullable Project project);

    void attachTask(@Nullable Project project, @Nullable Task task);

    void detachTask(@Nullable Project project, @Nullable Task task);

    void detachAllTasks(@Nullable Project project);

    boolean doesProjectExistByName(@Nullable String projectName);

    boolean doesProjectExistByNameAndUserId(@Nullable String projectName, @Nullable String userId);

    Project findOneByNamePart(String string);

    Project findOneByDescriptionPart(String string);

    List<Project> sortByCreationDate(List<Project> projectList);

    List<Project> sortByStartDate(List<Project> projectList);

    List<Project> sortByEndDate(List<Project> projectList);

    List<Project> sortByReadinessStatus(List<Project> projectList);
}
