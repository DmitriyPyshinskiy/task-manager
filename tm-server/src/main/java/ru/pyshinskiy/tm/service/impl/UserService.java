package ru.pyshinskiy.tm.service.impl;


import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.UserRepositoryMapper;
import ru.pyshinskiy.tm.service.IUserService;
import ru.pyshinskiy.tm.util.db.MyBatisConnection;

import java.util.List;
import java.util.Objects;


public final class UserService implements IUserService {

    public List<User> findAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            return mapper.findAll();
        }
    }

    public User findOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            return mapper.findOneById(id);
        }
    }

    public void persist(@Nullable User entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            mapper.persist(entity);
        }
    }

    public void merge(@Nullable String id, @Nullable User entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            mapper.merge(id, entity);
        }
    }

    public void removeAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            mapper.removeAll();
        }
    }

    public void removeOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            UserRepositoryMapper mapper = sqlSession.getMapper(UserRepositoryMapper.class);
            mapper.removeOneById(id);
        }
    }

    public boolean doesUserExistByLogin(@Nullable String userLogin) {
        return findAll().stream()
                .anyMatch(user -> Objects.requireNonNull(user.getLogin()).equals(userLogin));
    }

}
