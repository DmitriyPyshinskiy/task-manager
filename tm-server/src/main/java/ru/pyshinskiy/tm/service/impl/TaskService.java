package ru.pyshinskiy.tm.service.impl;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.TaskRepositoryMapper;
import ru.pyshinskiy.tm.service.ITaskService;
import ru.pyshinskiy.tm.util.db.MyBatisConnection;
import ru.pyshinskiy.tm.util.sort.SortMethods;

import java.util.List;
import java.util.Objects;

public final class TaskService implements ITaskService {


    public List<Task> findAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findAll();
        }
    }


    public List<Task> findAllByUserId(@Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findAllByUserId(userId);
        }
    }

    public Task findOneById(String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findOneById(id);
        }
    }


    public Task findOneByNameAndUserId(@Nullable String taskName, @Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findOneByNameAndUserId(taskName, userId);
        }
    }

    public void persist(Task entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            mapper.persist(entity);
        }
    }

    public void merge(String id, Task entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            mapper.merge(id, entity);
        }
    }

    public void removeAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            mapper.removeAll();
        }
    }

    public void removeAllByUserId(@Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            mapper.removeAllByUserId(userId);
        }
    }

    public void removeOneById(String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            mapper.removeOneById(id);
        }
    }

    public boolean doesTaskExistByName(@Nullable String taskName) {
        return findAll().stream()
                .anyMatch(task -> Objects.requireNonNull(task.getName()).equals(taskName));
    }

    public boolean doesTaskExistByNameAndUserId(@Nullable String taskName, @Nullable String userId) {
        return findAll().stream()
                .anyMatch(task -> Objects.requireNonNull(task.getName()).equals(taskName));
    }

    @Override
    public @Nullable Task findOneByNamePart(String namePart) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findOneByNamePart(namePart);
        }
    }

    @Override
    public @Nullable Task findOneByDescriptionPart(String descriptionPart) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            TaskRepositoryMapper mapper = sqlSession.getMapper(TaskRepositoryMapper.class);
            return mapper.findOneByNamePart(descriptionPart);
        }
    }


    public List<Task> sortByCreationDate(List<Task> taskList) {
        return SortMethods.sortByCreationDate(taskList);
    }

    public List<Task> sortByStartDate(List<Task> taskList) {
        return SortMethods.sortByStartDate(taskList);
    }

    public List<Task> sortByEndDate(List<Task> taskList) {
        return SortMethods.sortByEndDate(taskList);
    }

    public List<Task> sortByStatusReadiness(List<Task> taskList) {
        return SortMethods.sortByReadinessStatus(taskList);
    }

}
