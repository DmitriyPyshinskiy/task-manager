package ru.pyshinskiy.tm.service.impl;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.constant.SecurityConst;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.exception.IllegalLoginException;
import ru.pyshinskiy.tm.repository.SessionRepositoryMapper;
import ru.pyshinskiy.tm.service.ISessionService;
import ru.pyshinskiy.tm.service.IUserService;
import ru.pyshinskiy.tm.util.db.MyBatisConnection;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import java.util.List;
import java.util.Objects;

public final class SessionService implements ISessionService {
    private final IUserService userService;

    public SessionService(IUserService userService) {
        this.userService = userService;
    }

    public List<Session> findAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            return mapper.findAll();
        }
    }

    public Session findOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            return mapper.findOneById(id);
        }
    }

    public Session findOneByUserId(@Nullable String userId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            return mapper.findOneByUserId(userId);
        }
    }

    @Override
    public Session findOneByUserIdAndSessionId(String userId, String sessionId) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            return mapper.findAll().stream()
                    .filter(session -> Objects.requireNonNull(session.getUserId()).equals(userId)
                            && session.getId().equals(sessionId))
                    .findFirst().get();
        }
    }

    public Session persist(String login, String password) {
        SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession();
        SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
        Session session = createSession(login, password);
        try {
            mapper.persist(session);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
        return session;
    }

    public void merge(@Nullable String id, @Nullable Session entity) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            mapper.merge(id, entity);
            sqlSession.commit();
        }
    }

    public void removeAll() {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            mapper.removeAll();
        }
    }

    public void removeOneById(@Nullable String id) {
        try(SqlSession sqlSession = MyBatisConnection.getSqlSessionFactoryFormJava().openSession()) {
            SessionRepositoryMapper mapper = sqlSession.getMapper(SessionRepositoryMapper.class);
            mapper.removeOneById(id);
        }
    }

    @Override
    public boolean doesSessionExistByUserId(String userId) {
        return findAll().stream()
                .anyMatch(session -> Objects.requireNonNull(session.getUserId()).equals(userId));
    }

    private Session createSession(String login, String password) {
        for (User user : userService.findAll()) {
            if (Objects.requireNonNull(user.getLogin()).equals(login)
                    && Objects.requireNonNull(user.getHashPassword()).equals(password)) {

                Session session = new Session(user.getId(), user.getRole(), null);
                session.setSignature(SignatureUtil.sign(session, SecurityConst.SALT, SecurityConst.CYCLE));
                return session;
            }
        }
        throw new IllegalLoginException();
    }
}
