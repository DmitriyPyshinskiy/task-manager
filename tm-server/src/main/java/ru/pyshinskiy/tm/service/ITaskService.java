package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAllByUserId(@Nullable String userId);

    Task findOneById(@Nullable String id);

    Task findOneByNameAndUserId(@Nullable String taskName, @Nullable String userId);

    void persist(@Nullable Task entity);

    void merge(@Nullable String id, @Nullable Task entity);

    void removeAll();

    void removeAllByUserId(@Nullable String userId);

    void removeOneById(@Nullable String id);

    boolean doesTaskExistByName(@Nullable String taskName);

    boolean doesTaskExistByNameAndUserId(@Nullable String taskName, @Nullable String userId);

    Task findOneByNamePart(String namePart);

    Task findOneByDescriptionPart(String descriptionPart);

    List<Task> sortByCreationDate(List<Task> taskList);

    List<Task> sortByStartDate(List<Task> taskList);

    List<Task> sortByEndDate(List<Task> taskList);

    List<Task> sortByStatusReadiness(List<Task> taskList);
}
