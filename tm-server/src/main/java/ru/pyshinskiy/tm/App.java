package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        new Bootstrap().init();
    }
}
